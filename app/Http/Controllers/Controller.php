<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
	 * @param array $messages
	 * @throws \Illuminate\Validation\ValidationException
	 * @return void
	 */
    protected function throw_validation($messages)
    {
    	throw ValidationException::withMessages(
    		$messages
    	);
    }
}
