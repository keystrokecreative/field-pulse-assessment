<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Exceptions\Login\BadCredentials as BadLoginCredentials;
use App\Exceptions\Login\TooManyAttempts as TooManyLoginAttempts;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

/**
 * @Controller(prefix="api/sessions")
 * @Middleware("api")
 * @Middleware("web")
 */
class SessionsController extends Controller
{
	use AuthenticatesUsers;

    /**
     * Login user
     *
     * @Get("create/get", as="api::sessions.store.get")
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws BadLoginCredentials
     * @throws TooManyLoginAttempts
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
    public function test_get_store(Request $request)
    {
        return $this->store($request);
    }

    /**
     * Logout user
     *
     * @Get("destroy/get", as="api::sessions.destroy.get")
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws BadLoginCredentials
     * @throws TooManyLoginAttempts
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
    public function test_get_destroy(Request $request)
    {
        return $this->destroy($request);
    }

    /**
     * Login user
     *
     * @Post("create", as="api::sessions.store")
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws BadLoginCredentials
     * @throws TooManyLoginAttempts
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
	public function store(Request $request)
	{
        $input = $request->validate([
            'email'       => 'required|email|max:255',
            'password'    => 'required|string|max:255',
            'remember_me' => 'boolean',
        ]);

		// handle too many bad login attempts
        if ($this->hasTooManyLoginAttempts($request))
        {
            $this->fireLockoutEvent($request);

            $seconds = $this->locked_out_seconds_remaining(
            	$request
            );

	        $error = (new TooManyLoginAttempts)->set_seconds(
	        	$seconds
	        );
	        throw $error;
        }

        // find user, check credentials
        $user = User::where(
        	'email', $request->input('email')
        )->withTrashed()->first();

        if($user->deleted_at){
            return api()->error(403, 'account_deactivated', $user->toArray());
        }

        $is_correct = $user && \Hash::check(
            $request->input('password'),
            $user->password
        );

        // handle bad credentials
		if (!$is_correct) {
	        $this->incrementLoginAttempts($request);

			throw new BadLoginCredentials;
		}

		auth()->login(
			$user, (bool) $request->input('remember_me')
		);

		return $this->auth_response(
			'Login successful.'
		);
	}

    /**
     * Get current user
     *
     * @Get("current", as="api::sessions.show")
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
	public function current(Request $request)
	{
		return $this->auth_response();
	}

    /**
     * Update current auth user
     *
     * @Post("update", as="api::sessions.update")
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     * @throws \Illuminate\Validation\ValidationException
     */
	public function update(Request $request)
	{
        $auth = $this->auth();
		$input = $request->validate([
            'first_name' => 'string|max:255|required',
			'last_name'  => 'string|max:255|required',
            'email' 	 => 'email|max:255|unique:users,email,'.$auth->id,
        ]);

		$auth->update(
            $input
	    );

	    return $this->auth_response(
	    	'Account Updated.'
	    );
	}

    /**
     * Logout user
     *
     * @Post("destroy", as="api::sessions.destroy")
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
	public function destroy(Request $request)
	{
        $auth = $this->auth();

		auth()->logout();

		return api()->success(
			'Session destroyed.'
		);
	}

    /*
    |--------------------------------------------------------------------------
    | Login Throttling
    |--------------------------------------------------------------------------
    */

    /**
     * Allowed number of login attempts before account lock
     *
     * @var string
     */
    protected $maxAttempts = 5;

    /**
     * Account lock length in minutes after too many failed login attempts
     *
     * @var string
     */
    protected $decayMinutes = 5;

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return int
     */
    protected function locked_out_seconds_remaining($request)
    {
        return $this->limiter()->availableIn(
            $this->throttleKey($request)
        );
    }
}
