<?php

namespace App\Http\Controllers\API;

use App\Models\Client;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * @Controller(prefix="api/clients/{client_id}/users")
 * @Middleware("api")
 * @Middleware("web")
 */
class ClientUsersController extends Controller
{
    /**
     * Return a listing of the client's users.
     *
     * @Get("/", as="api::clients.users.index")
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Client $client_id
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthorized
     */
    public function index(Request $request, $client_id)
    {
        $client = $this->accessible_client($client_id);
        $request->validate([
            'search'    => 'string|nullable|max:255',
            'sort_by'   => 'string|nullable|in:first_name,last_name,email,total_hours_logged',
            'sort_dir'  => 'string|nullable|in:asc,desc',
            'page'      => 'integer|nullable|min:1',
            'per_page'  => 'integer|nullable|in:10,25,50,100',
        ]);

        $users = $client->users()->select_total_minutes_logged()->withTrashed()->search(
            $request->search
        )->sorted(
            $request->sort_by, $request->sort_dir
        )->paginate(
            $request->input('per_page', 10)
        );

        $data = $users->toArray();
        $data['users'] = $data['data'];
        unset($data['data']);

        return api()->success($data);
    }

    /**
     * Store a newly created user for the client in storage.
     *
     * @Post("/create", as="api::clients.users.store")
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Client $client_id
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthorized
     */
    public function store(Request $request, $client_id)
    {
        $client = $this->manageable_client($client_id);

        $input = $request->validate([
            'first_name' => 'string|max:255|required',
            'last_name'  => 'string|max:255|required',
            'email'      => 'email|max:255|unique:users,email|required',
        ]);

        $input['type'] = User::TypeClientUser;

        $user = $client->users()->create(
            $input
        );

        return api()->success(
            'User account created', [
                'user' => $user->toArray()
            ]
        );
    }

    /**
     * Return the specified user.
     *
     * @Get("/{user_id}", as="api::clients.users.show")
     *
     * @param \App\Models\Client $client_id
     * @param \App\Models\User $user_id
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthorized
     */
    public function show($client_id, $user_id)
    {
        $client = $this->accessible_client($client_id);
        $user = $client->users()->with('client')->with('time_logs')
        ->findOrFail($user_id);

        return api()->success(
            'Client user account details', [
                'user' => $user->toArray()
        ]);
    }

    /**
     * Update the specified user in storage.
     *
     * @Post("/{user_id}/edit", as="api::clients.users.update")
     *
     * @param \Illuminate\Http\Request $request
     * @param $client_id
     * @param $user_id
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
    public function update(Request $request, $client_id, $user_id)
    {
        $client = $this->manageable_client($client_id);

        $user = $client->users()->findorFail($user_id);

        if (!$this->auth()->can_manage_user($user)) {
            $this->unauthorized();
        }

        $input = $request->validate([
            'first_name' => 'string|max:255|required',
            'last_name'  => 'string|max:255|required',
            'email'      => 'email|max:255|unique:users,email,'.$user_id,
        ]);

        $user->update($input);

        return api()->success(
            'User account updated', [
                'user' => $user->toArray()
            ]
        );
    }

    /**
     * Deactivate the specified user account
     *
     * @Post("/{user_id}/destroy", as="api::clients.users.destroy")
     *
     * @param \App\Models\Client $client_id
     * @param $user_id
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
    public function destroy($client_id, $user_id)
    {
        $client = $this->manageable_client($client_id);

        $user = $client->users()->findOrFail($user_id);

        if (!$this->auth()->can_manage_user($user)) {
            $this->unauthorized();
        }

        $user->delete();

        return api()->success(
            'User account deactivated', [
                'user' => $user->toArray()
            ]
        );
    }

    /**
     * Reactivate the specified user account
     *
     * @Post("/{user_id}/restore", as="api::clients.users.restore")
     *
     * @param $client_id
     * @param $user_id
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
    public function restore($client_id, $user_id)
    {
        $client = $this->manageable_client($client_id);

        $user = $client->users()->withTrashed()->findOrFail($user_id);

        if (!$this->auth()->can_manage_user($user)) {
            $this->unauthorized();
        }

        $user->restore();

        return api()->success(
            'User account reactivated', [
                'user' => $user->toArray()
            ]
        );
    }
}
