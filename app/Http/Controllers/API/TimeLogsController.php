<?php

namespace App\Http\Controllers\API;

use App\Models\TimeLog;
use App\Models\Client;
use Illuminate\Http\Request;

/**
 * @Controller(prefix="api/time-logs")
 * @Middleware("api")
 * @Middleware("web")
 */
class TimeLogsController extends Controller
{
    /**
     * List time logs for sys_admin/client/user
     *
     * @Get("/", as="api::time-logs.index")
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     */
    public function index(Request $request)
    {
        $request->validate([
            'search'    => 'string|nullable|max:255',
            'sort_by'   => 'string|nullable|in:started_at,ended_at,user,client,duration',
            'sort_dir'  => 'string|nullable|in:asc,desc',
            'page'      => 'integer|nullable|min:1',
            'per_page'  => 'integer|nullable|in:10,25,50,100',
        ]);

        $auth =  $this->auth();
        $data = collect();

        if($auth->is_client_user)
        {
            $data = $auth->time_logs()->withTrashed()->search(
                    $request->search
                )->sorted(
                    $request->sort_by, $request->sort_dir
                )->paginate(
                    $request->input('per_page', 10)
                );
        }

        if($auth->is_client_admin) {
            $data = TimeLog::whereHas('client', function($client) use($auth){
                $client->where('clients.id', $auth->client->id);
            })->search(
                $request->search
            )->sorted(
                $request->sort_by, $request->sort_dir
            )->paginate(
                $request->input('per_page', 10)
            );
        }

        if($auth->is_sys_admin)
        {
            $data =  TimeLog::withTrashed()->with('user.client')->search(
                    $request->search
                )->sorted(
                    $request->sort_by, $request->sort_dir
                )->paginate(
                    $request->input('per_page', 10)
                );
        }

        return api()->success($data->toArray());
    }

    /**
     * Store new time log
     *
     * @Post("/create", as="api::time-logs.store")
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     */
    public function store(Request $request)
    {
        $auth = $this->auth();

        if(!$auth->is_client_user && !$auth->is_client_admin)
        {
            return api()->error('Must be user of type Client User or Client Admin', 403);
        }

        if($auth->open_time_log()->count()){
            return api()->error('You already have an open time log', 400, $auth->open_time_log);
        }

        $input = $request->validate([
            'started_at'    => 'date_format:Y-m-d H:i:s',
            'ended_at'      => 'nullable|date_format:Y-m-d H:i:s'
        ]);

        $input = array_merge($input, [
            'user_id'   =>  $auth->id
        ]);

        $time_log  = TimeLog::create($input);

        return api()->success('TimeLog created', [
            $time_log
        ]);
    }

    /**
     * Return specified time log
     *
     *@Get("/{time_log_id}", as="api::time-logs.show")
     *
     * @param $time_log_id
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
    public function show($time_log_id)
    {
        $auth = $this->auth();

        $time_log = TimeLog::with('user.client')->findOrFail($time_log_id);

        if ($auth->id !== $time_log->user_id && !$this->auth()->can_manage_user($time_log->user)) {
            $this->unauthorized();
        }

        return api()->success($time_log->toArray());
    }

    /**
     * Update the specified time log in storage.
     *
     * @Post("/{time_log_id}/edit", as="api::time-logs.update")
     *
     * @param Request $request
     * @param $time_log_id
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
    public function update(Request $request, $time_log_id)
    {
        $input = $request->validate([
            'started_at'    => 'nullable|date_format:Y-m-d H:i:s',
            'ended_at'      => 'nullable|date_format:Y-m-d H:i:s'
        ]);

        $time_log = TimeLog::findOrFail($time_log_id);

        $auth = $this->auth();

        if ($auth->id !== $time_log->user_id && !$this->auth()->can_manage_user($time_log->user)) {
            $this->unauthorized();
        }

        $time_log->update($input);

        return api()->success('Time Log Updated', [
            'time_log'  =>  $time_log->toArray()
            ]
        );
    }

    /**
     * Remove the specified time log from storage.
     *
     * @Post("/{time_log_id}/destroy", as="api::time-logs.destroy")
     *
     * @param $time_log_id
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
    public function destroy($time_log_id)
    {
        $time_log = TimeLog::findOrFail($time_log_id);

        $auth = $this->auth();

        if ($auth->id !== $time_log->user_id && !$this->auth()->can_manage_user($time_log->user)) {
            $this->unauthorized();
        }

        $time_log->delete();

        return api()->success('Time Log Removed', [
            'time_log' => $time_log->toArray()
            ]
        );
    }
}
