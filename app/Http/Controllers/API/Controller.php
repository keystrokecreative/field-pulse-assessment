<?php

namespace App\Http\Controllers\API;

use App\Exceptions\Unauthenticated;
use App\Models\Client;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    /**
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     */
    protected function auth()
    {
        if (!auth()->check()) {
            $this->unauthenticated();
        }

        return auth()->user();
    }

    /**
     * @param mixed $client
     * @return \App\Models\Client
     * @throws \App\Exceptions\Unauthorized
     */
    protected function accessible_client($client = null)
    {
        $auth = $this->auth();

        if (!isset($client)) {
            $client = $auth->client()->first();
        }

        if (!isset($client)) {
            $this->not_found();
        }

        if (is_numeric($client)) {
            $client = Client::withTrashed()->findOrFail(
                $client
            );
        }

        if (!$auth->can_access_client($client)) {
            $this->not_found();
        }

        return $client;
    }

    /**
     * @param mixed $client
     * @return \App\Models\Client
     * @throws \App\Exceptions\Unauthorized
     */
    protected function manageable_client($client = null)
    {
        $auth = $this->auth();

        $client = $this->accessible_client($client);

        if (!$auth->can_manage_client($client)) {
            $this->unauthorized();
        }

        return $client;
    }

    /**
     * @return void
     * @throws \App\Exceptions\Unauthorized
     */
    protected function unauthorized()
    {
        throw new \App\Exceptions\Unauthorized();
    }

    /**
     * @return void
     * @throws \App\Exceptions\Unauthenticated
     */
    protected function unauthenticated()
    {
        throw new \App\Exceptions\Unauthenticated();
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function not_found()
    {
        abort(404);
    }

    /**
     * @param array $data
     * @param null $message
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     */
    protected function auth_response($data = [], $message = null)
    {
    	$auth = auth()->user();

    	if(!$auth){
            $this->unauthenticated();
        }

    	if($auth->is_client_user){
    	    $auth->loadMissing([
    	        'time_logs',
                'client'
            ]);
        }

    	if($auth->is_client_admin){
    	    $auth->loadMissing([
    	        'client'
            ]);
        }

        if (is_string($data)) {
            $message = $data;
            $data = [];
        }

    	return api()->success(
    		$message, array_merge(
                $data, ['auth' => $auth->toArray()]
            )
    	);
    }
}
