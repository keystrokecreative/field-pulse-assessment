<?php

namespace App\Http\Controllers\API;

use App\Models\Client;
use Illuminate\Http\Request;

/**
 * @Controller(prefix="api/clients")
 * @Middleware("api")
 * @Middleware("web")
 */
class ClientsController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @Post("/{client_id}/edit", as="api::clients.update")
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $client_id)
    {
        $client = $this->manageable_client($client_id);

        $input = $request->validate([
            'name' => 'string|max:255',
        ]);

        $client->update($input);

        return api()->success(
            'Client account updated', [
                'client' => $client->toArray()
            ]
        );
    }

    /**
     * Deactivate the specified client account
     *
     * @Post("/{client_id}/destroy", as="api::clients.destroy")
     *
     * @param  \App\Models\Client  $client_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($client_id)
    {
        $client = $this->manageable_client($client_id);

        $client->delete();

        return api()->success(
            'Client account deactivated', [
                'client' => $client->toArray()
            ]
        );
    }

    /**
     * Reactivate the specified client account
     *
     * @Post("/{client_id}/restore", as="api::clients.restore")
     *
     * @param  int  $client_id
     * @return \Illuminate\Http\Response
     */
    public function restore($client_id)
    {
        $client = $this->manageable_client($client_id);

        $client->restore();

        return api()->success(
            'Client account reactivated', [
                'client' => $client->toArray()
            ]
        );
    }
}
