<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Hash;
use Password;
use Validator;

/**
 * @Controller(prefix="api/passwords")
 * @Middleware("api")
 * @Middleware("web")
 */
class PasswordsController extends Controller
{
	use ResetsPasswords;

    /**
     * Request password reset email
     *
     * @Post("/remind", as="api::passwords.remind")
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
	public function remind(Request $request)
	{
		$input = $request->validate([
            'email' => 'required|email|max:255',
        ]);

        // check email exists in users db
        $validator = Validator::make($request->input(), [
            'email' => 'exists:users',
        ]);

        if ($validator->fails()) {
            $this->throw_validation([
                'email' => __('passwords.user'),
            ]);
        }

        // check user w/ email has active account (not trashed)
        $user = \App\Models\User::where(
            'email', $request->email
        )->withTrashed()->first();
        if ($user && $user->trashed()) {
            $this->throw_validation([
                'email' => __('auth.inactive'),
            ]);
        }

        // check user w/ email has registered account
        if ($user && !$user->is_registered) {
            $this->throw_validation([
                'email' => __('auth.unregistered'),
            ]);
        }

//		$response = $this->broker()->sendResetLink(
//            $request->only('email')
//        );

//        if ($response != Password::RESET_LINK_SENT) {
//        	// handle error if needed
//            $this->throw_validation([
//                'email' => __($response),
//            ]);
//        }

        return api()->success(
    		"Password reset link sent"
		);
	}

    /**
     * Reset password from email link
     *
     * @Post("/reset", as="api::passwords.reset")
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
	public function post_reset(Request $request)
	{
        return $this->reset($request);
	}

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token'    => 'required|string|max:255',
            'email'    => 'required|email|max:255',
            'password' => 'required|string|confirmed|min:8|max:255',
        ];
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param \Illuminate\Http\Request $request
     * @param string $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\Unauthenticated
     */
    protected function sendResetResponse(Request $request, $response)
    {
        $auth = $this->auth();

    	return api()->success(
			trans($response)
		);
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param \Illuminate\Http\Request $request
     * @param string $response
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        $this->throw_validation([
            'email' => [trans($response)],
        ]);
    }

    /**
     * Update auth password
     *
     * @Post("/update", as="api::passwords.update")
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \App\Exceptions\Unauthenticated
     * @throws \App\Exceptions\Unauthorized
     * @throws \Illuminate\Validation\ValidationException
     */
	public function update(Request $request)
	{
        $auth = $this->auth();

		$input = $request->validate([
            'current_password' => 'required|string|max:255',
            'password' 		   => 'required|string|max:255',
        ]);

		$current_is_correct = Hash::check(
            $input['current_password'],
            $auth->password
        );
		if (!$current_is_correct) {
            $this->throw_validation([
                'current_password' => 'Incorrect value provided for current password',
            ]);
        }

        $auth->password = bcrypt(
            $input['password']
        );
        $auth->update();


		return api()->success(
			'Password updated'
		);
	}
}
