<?php

namespace App\Http;

use Illuminate\Support\Str;

class API
{
    /*
    |--------------------------------------------------------------------------
    | JSON Http Responses
    |--------------------------------------------------------------------------
    */

    /**
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    public function should_respond($request)
    {
        if ($request->expectsJson()) {
            return true;
        }

        $route = $request->route();
        if (!$route) {
            return Str::startsWith(
                $request->path(), 'api/'
            );
        }

        return collect(
            $route->middleware()
        )->contains('api');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function unauthenticated_error()
    {
        $message = static::ErrorUnauthenticated;

        return $this->response(
            'error', [
                $message,
                static::error_status_for($message),
            ]
        );
    }

    /**
     * @param array $args
     * @return \Illuminate\Http\Response
     */
    public function error(...$args)
    {
        return $this->response(
            'error', $args
        );
    }

    /**
     * @param array $args
     * @return \Illuminate\Http\Response
     */
    public function success(...$args)
    {
        return $this->response(
            'success', $args
        );
    }

    /**
     * @param string $type
     * @param array $args
     * @return \Illuminate\Http\Response
     */
    protected function response($type, $args)
    {
        $status = 200;
        $message = null;
        $data = [];

        foreach ($args as $arg) {
            if (is_string($arg)) {
                $message = $arg;
            }
            else if (is_numeric($arg)) {
                $status = $arg;
            }
            else {
                $data = $arg;
            }
        }

        $payload = [
            $type => [
                'status'  => $status,
                'message' => $message,
                'data'    => $data,
            ],
        ];

        return response()->json($payload, $status);
    }

    /*
    |--------------------------------------------------------------------------
    | Error Handling & HTTP Responses
    |--------------------------------------------------------------------------
    */

    const ErrorServerError = 'server_error';
    const ErrorNotFound = 'not_found';
    const ErrorMethodNotAllowed = 'method_not_allowed';
    const ErrorMaintenanceMode = 'system_maintenance';
    const ErrorTooManyAttempts = 'too_many_attempts';

    const ErrorUnauthenticated = 'unauthenticated';
    const ErrorUnauthorized = 'restricted_access';
    const ErrorLoginBadCredentials = 'bad_credentials';
    const ErrorLoginTooManyAttempts = 'too_many_bad_attempts';
    const ErrorUserAccountDeleted = 'account_deleted';

    const ErrorInputFormat = 'invalid_input_format';

    const ErrorEmailTaken = 'email_already_in_use';

    const ErrorPasswordResetToken = 'invalid_or_expired_reset_token';

    const ErrorPasswordIncorrect = 'incorrect_password';

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function handle($exception, $request)
    {
        $message = $this->error_message_for(
            $exception
        );

        $status = $this->error_status_for(
            $message
        );

        $data = $this->error_data_for(
            $request, $exception, $message
        );

        return $this->error(
            $message, $status, $data
        );
    }

    /**
     * @param  \Exception  $exception
     * @return string
     */
    protected function error_message_for($exception)
    {
        switch (get_class($exception))
        {
            case \App\Exceptions\Unauthorized::class:
                return static::ErrorUnauthorized;

            case \App\Exceptions\AccountDeleted::class:
            case \App\Exceptions\Unauthenticated::class:
            case \Illuminate\Auth\AuthenticationException::class:
            // case \League\OAuth2\Server\Exception\OAuthServerException::class:
                return static::ErrorUnauthenticated;

            case \App\Exceptions\EmailTaken::class:
                return static::ErrorEmailTaken;

            case \App\Exceptions\Login\BadCredentials::class:
                return static::ErrorLoginBadCredentials;
            case \App\Exceptions\Login\TooManyAttempts::class:
                return static::ErrorLoginTooManyAttempts;

            case \App\Exceptions\Passwords\Incorrect::class:
                return static::ErrorPasswordIncorrect;
            case \App\Exceptions\Passwords\BadResetToken::class:
                return static::ErrorPasswordResetToken;

            case \Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class:
            case \Illuminate\Database\Eloquent\ModelNotFoundException::class:
                return static::ErrorNotFound;

            case \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException::class:
                return static::ErrorMethodNotAllowed;

            case \Illuminate\Http\Exceptions\ThrottleRequestsException::class:
                return static::ErrorTooManyAttempts;

            case \Illuminate\Foundation\Http\Exceptions\MaintenanceModeException::class:
                case
                    \Symfony\Component\HttpKernel\Exception\HttpException::class:
                        return static::ErrorMaintenanceMode;
        }

        if ($exception instanceof \App\Exceptions\InvalidInput
            || $exception instanceof \Illuminate\Validation\ValidationException
            || $exception instanceof \Illuminate\Foundation\Validation\ValidationException) {
            return static::ErrorInputFormat;
        }

        return static::ErrorServerError;
    }

    /**
     * @param  string  $message
     * @return int
     */
    protected function error_status_for($message)
    {
        switch ($message)
        {
            case static::ErrorUnauthenticated:         return 401;

            case static::ErrorUnauthorized:
            case static::ErrorUserAccountDeleted:
            case static::ErrorPasswordIncorrect:
            case static::ErrorPasswordResetToken:
            case static::ErrorLoginBadCredentials:     return 403;

            case static::ErrorNotFound:                return 404;

            case static::ErrorMethodNotAllowed:        return 405;

            case static::ErrorInputFormat:             return 422;

            case static::ErrorEmailTaken:              return 409;

            case static::ErrorTooManyAttempts:
            case static::ErrorLoginTooManyAttempts:    return 429;

            case static::ErrorMaintenanceMode:         return 503;

            case static::ErrorServerError:
            default:                                   return 500;
        }
    }



    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @param  string  $error
     * @return array
     */
    protected function error_data_for($request, $exception, $error)
    {
        if ($exception instanceof \App\Exceptions\EmailTaken)
        {
            return $request->only('email');
        }

        if ($exception instanceof \App\Exceptions\Login\TooManyAttempts) {
            return [
                'seconds' => $exception->seconds(),
            ];
        }

        if ($exception instanceof \App\Exceptions\InvalidInput) {
            return $exception->validation_errors();
        }

        if ($exception instanceof \Illuminate\Validation\ValidationException
            || $exception instanceof \Illuminate\Foundation\Validation\ValidationException)
        {
            return $exception->validator->errors();
        }

        if (config('app.debug')
            && $error === static::ErrorServerError)
        {
            return [
                'message' => $exception->getMessage(),
                'stack'   => (string) $exception,
            ];
        }

        return [];
    }
}
