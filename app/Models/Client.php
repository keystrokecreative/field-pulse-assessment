<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at', 
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $withCount = [
        'users',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'is_active',
    ];

    /**
     * Eloquent Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(
            User::class, 'client_id'
        );
    }

    /**
     * Eloquent Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function time_logs()
    {
        return $this->hasManyThrough(
        	TimeLog::class,
            User::class
        );
    }

    /**
     * @return bool
     */
    public function getIsActiveAttribute()
    {
        return !$this->trashed();
    }
}
