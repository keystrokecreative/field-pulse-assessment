<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'type',
        'email_verified_at',
        'minutes_logged',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at', 
        'email_verified_at',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'client',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'is_active',
        'is_registered',
        'status',
        'account_type',
        'total_hours_logged',
    ];

    /**
     * Eloquent Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(
            Client::class, 'client_id'
        );
    }

    /**
     * Eloquent Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function open_time_log()
    {
        return $this->hasOne(
            TimeLog::class
        )->open();
    }

    /**
     * Eloquent Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function time_logs()
    {
        return $this->hasMany(
            TimeLog::class
        );
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $match
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $match)
    {
        if (!$match) {
            return $query;
        }

        $match = '%'.$match.'%';
        return $query->where(function($users) use ($match) {
            $users->where(
                'first_name', 'LIKE', $match
            )->orWhere(
                'last_name', 'LIKE', $match
            )->orWhere(
                'email', 'LIKE', $match
            )->orWhereRaw(
                "CONCAT(first_name, ' ', last_name) LIKE '{$match}'"
            );
        });
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $sort_by
     * @param string $sort_dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSorted($query, $sort_by, $sort_dir)
    {
        if (!$sort_by) {
            return $query;
        }

        switch ($sort_by) {
            case 'total_hours_logged':
                return $query->orderBy(
                    \DB::raw('SUM(TIMESTAMPDIFF(MINUTE,time_logs.started_at,IFNULL(time_logs.ended_at, NOW())))'), 
                    $sort_dir == 'asc' ? 'desc' : 'asc'
                );
        }

        return $query->orderBy(
            $sort_by, $sort_dir
        );
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSelect_total_minutes_logged($query)
    {
        return $query->join(
            'time_logs', 'users.id', '=', 'time_logs.user_id'
        )->addSelect(
            \DB::raw(
                'users.*, CAST(SUM(TIMESTAMPDIFF(MINUTE,time_logs.started_at,IFNULL(time_logs.ended_at, NOW()))) as UNSIGNED) as minutes_logged'
            )
        );
    }

    /**
     * @return float
     */
    public function getTotalMinutesLoggedAttribute()
    {
        if ($this->minutes_logged) {
            return $this->minutes_logged;
        }
        return (int) $this->time_logs()->sum(
            \DB::raw('TIMESTAMPDIFF(MINUTE, started_at, IFNULL(ended_at, NOW()))')
        );
    }

    /**
     * @return float
     */
    public function getTotalHoursLoggedAttribute()
    {
        return (float) number_format($this->total_minutes_logged / 60, 2);
    }

    /*
    |--------------------------------------------------------------------------
    | Status
    |--------------------------------------------------------------------------
    */

    /**
     * @return bool
     */
    public function getIsRegisteredAttribute()
    {
        return isset($this->password);
    }

    /**
     * @return bool
     */
    public function getIsActiveAttribute()
    {
        return $this->is_registered 
            && !$this->trashed();
    }

    /**
     * @return bool
     */
    public function getStatusAttribute()
    {
        if ($this->trashed()) {
            return 'Deactivated';
        }

        if (!$this->is_registered) {
            return 'Invited';
        }

        return 'Active';
    }

    /*
    |--------------------------------------------------------------------------
    | Type
    |--------------------------------------------------------------------------
    */

    const TypeClientUser = 1;
    const TypeClientAdmin = 2;
    const TypeSysAdmin = 3;

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function types()
    {
        return collect([
            static::TypeClientUser  => 'Client User',
            static::TypeClientAdmin => 'Client Admin',
            static::TypeSysAdmin    => 'System Admin',
        ]);
    }

    /**
     * @return string
     */
    public function getAccountTypeAttribute()
    {
        return static::types()->get($this->type);
    }

    /**
     * @return bool
     */
    public function getIsClientUserAttribute()
    {
        return $this->type == static::TypeClientUser;
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClient_users($query)
    {
        return $query->where(
            'type', static::TypeClientUser
        );
    }

    /**
     * @return bool
     */
    public function getIsClientAdminAttribute()
    {
        return $this->type == static::TypeClientAdmin;
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClient_admins($query)
    {
        return $query->where(
            'type', static::TypeClientAdmin
        );
    }

    /**
     * @return bool
     */
    public function getIsSysAdminAttribute()
    {
        return $this->type == static::TypeSysAdmin;
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSys_admins($query)
    {
        return $query->where(
            'type', static::TypeSysAdmin
        );
    }

    /**
     * @return bool
     */
    public function can_access_client($client)
    {
        if ($this->is_sys_admin) {
            return true;
        }

        return $this->client_id == $client->id;
    }

    /**
     * @return bool
     */
    public function can_manage_client($client)
    {
        if (!$this->can_access_client($client)) {
            return false;
        }

        return $this->is_sys_admin 
            || $this->is_client_admin;
    }

    /**
     * @return bool
     */
    public function can_manage_user($user)
    {
        if ($user->id == $this->id) {
            return false;
        }

        if ($user->is_sys_admin) {
            return false;
        }

        if ($this->is_sys_admin) {
            return true;
        }

        if ($user->is_client_admin) {
            return false;
        }

        if ($this->is_client_admin && $user->client_id == $this->client_id) {
            return true;
        }

        return false;
    }
}
