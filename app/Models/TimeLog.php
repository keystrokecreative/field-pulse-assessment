<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeLog extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'started_at',
        'ended_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'started_at', 
        'ended_at', 
        'deleted_at', 
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'is_archived',
        'duration',
    ];

    /**
     * Eloquent Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class, 'user_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function client()
    {
        return $this->hasOneThrough(
            Client::class,
            User::class,
            'id',
            'id',
            'user_id',
            'client_id'
        );
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $match
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $match)
    {
        return $query->whereHas('user', function($user) use ($match) {
            $user->search($match);
        });
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $sort_by
     * @param string $sort_dir
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSorted($query, $sort_by, $sort_dir)
    {
        if (!$sort_by) {
            return $query;
        }
        
        switch ($sort_by) {
            case 'duration': 
                return $query->orderBy(
                    \DB::raw('TIMESTAMPDIFF(MINUTE, started_at, IFNULL(ended_at, NOW()))'), 
                    $sort_dir == 'asc' ? 'asc' : 'desc'
                );
            case 'user': 
                return $query->join(
                    'users', 'time_logs.user_id', 'users.id'
                )->orderBy(
                    'users.last_name', $sort_dir
                );
            case 'client': 
                return $query->join(
                    'users', 'time_logs.user_id', 'users.id'
                )->join(
                    'clients', 'users.client_id', 'clients.id'
                )->orderBy(
                    'clients.name', $sort_dir
                );
        }

        return $query->orderBy(
            $sort_by, $sort_dir
        );
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSelect_total_minutes_logged($query)
    {
        return $query->addSelect(
            \DB::raw(
                'time_logs.*, CAST(SUM(TIMESTAMPDIFF(MINUTE,started_at,IFNULL(ended_at, NOW()))) as UNSIGNED) as total_minutes'
            )
        )->groupBy('time_logs.id');
    }

    /**
     * @return integer
     */
    public function getTotalMinutesAttribute()
    {
        // if ($this->is_open) {
        //     return;
        // }

        return ($this->ended_at ?: now())->diffInMinutes(
            $this->started_at
        );
    }

    /**
     * @return float
     */
    public function getTotalHoursAttribute()
    {
        // if ($this->is_open) {
        //     return;
        // }

        return ($this->ended_at ?: now())->diffInHours(
            $this->started_at
        ) + ($this->total_minutes % 60 / 60);
    }

    /**
     * @return float
     */
    public function getDurationAttribute()
    {
        $duration = '';
        $hours = floor($this->total_hours);
        if ($hours > 1) {
            $duration = $hours . ' hrs';
        }
        else if ($hours > 0) {
            $duration = $hours . ' hr';
        }


        $minutes = $this->total_minutes % 60;
        if ($minutes > 0 && $hours > 0) {
            $duration .= ' ';
        }


        if ($minutes > 1) {
            $duration .= $minutes . ' mins';
        }
        else if ($minutes > 0) {
            $duration .= $minutes . ' min';
        }

        return $duration;
    }

    /*
    |--------------------------------------------------------------------------
    | Status
    |--------------------------------------------------------------------------
    */

    /**
     * @return bool
     */
    public function getIsArchivedAttribute()
    {
        return $this->trashed();
    }

    /**
     * @return bool
     */
    public function getIsOpenAttribute()
    {
        return !isset($this->ended_at);
    }

    /**
     * Query Scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $is_open
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOpen($query, $is_open = true)
    {
        return $query->{
            $is_open ? 'whereNull' : 'whereNotNull'
        }('ended_at');
    }
}
