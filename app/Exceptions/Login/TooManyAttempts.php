<?php

namespace App\Exceptions\Login;

use Exception;

class TooManyAttempts extends Exception
{
	/**
     * @var int
     */
	protected $seconds;

	/**
     * @param int $seconds
     * @return \App\Exceptions\Login\TooManyAttempts
     */
    public function set_seconds($seconds)
    {
    	$this->seconds = $seconds;

    	return $this;
    }

    /**
     * @return int
     */
    public function seconds()
    {
    	return $this->seconds;
    }
}