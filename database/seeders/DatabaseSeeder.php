<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\TimeLog;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->unlock_db();

        TimeLog::truncate();
        User::truncate();
        Client::truncate();

        $this->faker = Faker::create();

    	$this->new_user(
    		User::TypeSysAdmin, 'admin@fieldpulse.com'
    	)->save();
        
        $client = Client::create([
        	'name' => 'Acme, Inc.',
        ]);

        $client->users()->save(
        	$this->new_user(
	    		User::TypeClientAdmin, 'admin@acme.com'
	    	)
        );
        $client->users()->save(
        	$this->new_user(User::TypeClientAdmin)
        );

        $client->users()->save(
        	$this->new_user(
	    		User::TypeClientUser, 'user@acme.com'
	    	)
        );

        foreach (range(1,10) as $i) {
        	$client->users()->save(
	        	$this->new_user(User::TypeClientUser)
	        );
        }

        foreach ($client->users()->get() as $user) {
        	$this->add_time_logs_for_user($user);
        }

        foreach (range(1,2) as $i) {
	        $unregistered = $client->users()->save(
	        	$this->new_user(User::TypeClientUser)
	        );
	        $unregistered->password = null;
	        $unregistered->update();
	    }

        foreach (range(1,2) as $i) {
	        $deactivated = $client->users()->save(
	        	$this->new_user(User::TypeClientUser)
	        );
	        $deactivated->delete();
	    }

        $this->lock_db();
    }

    /**
     * @param App\Models\User $user
     * @return void
     */
    protected function add_time_logs_for_user($user)
    {
    	$user_time_logs = [];

    	$start_logging_at = \Carbon\Carbon::now()->subMonths(
    		collect(range(1,3))->random()
    	);

    	$user->created_at = $start_logging_at;

    	$total_days = \Carbon\Carbon::now()->diffInDays($start_logging_at);
    	foreach (range(1,$total_days) as $day) 
    	{
    		if ($this->faker->boolean(2/7 * 100)) {
    			continue;
    		}

    		$started_at = $start_logging_at->copy()->addDays($day-1);
    		$started_at->hour = collect(range(7,14))->random() - 5;
    		$started_at->minute = collect(range(0,59))->random();

    		$total_hours = collect(range(4,9))->random();
    		$ended_at = $started_at->copy()->addHours($total_hours);
    		$ended_at->minute = collect(range(0,59))->random();

    		if ($day == $total_days && $this->faker->boolean()) {
    			$ended_at = null;
    		}

    		$user_time_logs[] = new TimeLog([
    			'started_at' => $started_at,
    			'ended_at' 	 => $ended_at,
    		]);
    	}

    	$user->time_logs()->saveMany($user_time_logs);
    }

    /**
     * @param integer $type
     * @param string $email
     * @return App\Models\User
     */
    protected function new_user($type, $email = null)
    {
        return new User([
    		'first_name' 		=> $this->faker->firstName,
    		'last_name'  		=> $this->faker->lastName,
    		'email' 	 		=> $email ?: $this->faker->email,
    		'type'		 		=> $type,
            'email_verified_at' => now(),
            'password' 			=> bcrypt('secret123'), // password
            'remember_token' 	=> Str::random(10),
    	]);
    }

    /**
     * @return Wripple
     */
    protected function unlock_db()
    {
        \Illuminate\Database\Eloquent\Model::unguard();

        if (config('database.default') === 'mysql') {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        return $this;
    }

    /**
     * @return Wripple
     */
    protected function lock_db()
    {
        if (config('database.default') === 'mysql') {
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

        \Illuminate\Database\Eloquent\Model::reguard();

        return $this;
    }
}
